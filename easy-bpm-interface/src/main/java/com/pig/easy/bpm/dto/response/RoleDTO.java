package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/14 10:50
 */
@Data
@ToString
public class RoleDTO extends BaseResponseDTO {

    private static final long serialVersionUID = 8860971765358100612L;

    private Long roleId;

    private String roleCode;

    private String roleName;

    private Integer roleLevel;

    private String roleAbbr;

    private String roleAliasName;

    private String tenantId;

    private Long companyId;

    private Long deptId;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

}
