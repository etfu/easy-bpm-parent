package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/28 14:58
 */
@Data
@ToString
public class FormReqDTO extends BaseRequestPageDTO {

    private static final long serialVersionUID = 6579568446090674467L;

    private Long formId;

    private String formKey;

    private String formName;

    private String tenantId;

    private Integer formType;

    private Integer sort;

    private String formData;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;
}
