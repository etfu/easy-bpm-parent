package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/24 10:47
 */
@Data
@ToString
public class RoleGroupQueryDTO extends BaseRequestDTO {

    private static final long serialVersionUID = -2860696528734758341L;

    private Long roleGroupId;

    /**
     * 角色组编码
     */
    private String roleGroupCode;

    /**
     * 角色组名称
     */
    private String roleGroupName;

    /**
     * 角色组简称
     */
    private String roleGroupAbbr;

    /**
     * 角色组等级
     */
    private Integer roleGroupLevel;

    /**
     * 角色组类别
     */
    private Integer roleGroupType;

    /**
     * 所属条线
     */
    private String businessLine;

    /**
     * 租户编号
     */
    private String tenantId;

}
