package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/27 14:22
 */
@Data
@ToString
public class DeptSaveOrUpdateDTO extends BaseRequestDTO {

    private static final long serialVersionUID = 2760785675055494945L;

    private Long deptId;

    private String deptCode;

    private String deptName;

    private Long companyId;

    private String companyCode;

    private String tenantId;

    private Long deptParentId;

    private String deptParentCode;

    private Integer deptLevel;

    private String deptType;

    private String deptTypeCode;

    private String remark;

    private Integer deptOrder;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private String businessLine;
}
