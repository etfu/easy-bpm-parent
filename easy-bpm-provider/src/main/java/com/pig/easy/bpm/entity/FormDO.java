package com.pig.easy.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("bpm_form")
public class FormDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表单编号
     */
    @TableId(value = "form_id", type = IdType.AUTO)
    private Long formId;
    /**
     * 表单KEY
     */
    @TableField("form_key")
    private String formKey;
    /**
     * 表单名称
     */
    @TableField("form_name")
    private String formName;
    /**
     * 租户编号
     */
    @TableField("tenant_id")
    private String tenantId;

    /**
     * 表单类型 1 pc 2 移动端
     */
    @TableField("form_type")
    private Integer formType;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 表单json数据
     */
    @TableField("form_data")
    private String formData;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 状态 1 有效 0 失效
     */
    @TableField("valid_state")
    private Integer validState;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;
    /**
     * 操作人工号
     */
    @TableField("operator_id")
    private Long operatorId;
    /**
     * 操作人姓名
     */
    @TableField("operator_name")
    private String operatorName;


}
