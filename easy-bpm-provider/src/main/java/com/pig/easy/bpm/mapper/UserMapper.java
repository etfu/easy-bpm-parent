package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.dto.request.UserQueryDTO;
import com.pig.easy.bpm.dto.response.UserInfoDTO;
import com.pig.easy.bpm.entity.UserDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-14
 */
@Mapper
public interface UserMapper extends BaseMapper<UserDO> {

    @Select("select count(1)+1 from bpm_user")
    Long getUserId();

    UserDO getUser(@Param("userName") String userName);

    List<UserInfoDTO> getListByCondition(UserQueryDTO userQueryDTO);
}
