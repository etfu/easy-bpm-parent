package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.VariableDictQueryDTO;
import com.pig.easy.bpm.dto.request.VariableDictSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.VariableDictDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.VariableDictService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.VariableDictQueryVO;
import com.pig.easy.bpm.vo.request.VariableDictSaveOrUpdateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * <p>
 * 变量表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-07-09
 */
@RestController
@Api(tags = "变量表管理", value = "变量表管理")
@RequestMapping("/variableDict")
public class VariableDictController extends BaseController {

        @Reference
        VariableDictService service;

        @ApiOperation(value = "查询变量表列表", notes = "查询变量表列表", produces = "application/json")
        @PostMapping("/getList")
        public JsonResult getList(@Valid @RequestBody VariableDictQueryVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmptyByDefault(param.getTenantId());
        VariableDictQueryDTO queryDTO = switchToDTO(param, VariableDictQueryDTO.class);

        Result<PageInfo<VariableDictDTO>> result = service.getListByCondition(queryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "新增变量表", notes = "新增变量表", produces = "application/json")
        @PostMapping("/insert")
        public JsonResult insertVariableDict(@Valid @RequestBody VariableDictSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
       VariableDictSaveOrUpdateDTO saveDTO = switchToDTO(param, VariableDictSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.insertVariableDict(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "修改变量表", notes = "修改变量表", produces = "application/json")
        @PostMapping("/update")
        public JsonResult updateVariableDict(@Valid @RequestBody VariableDictSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
         VariableDictSaveOrUpdateDTO saveDTO = switchToDTO(param, VariableDictSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.updateVariableDict(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

        @ApiOperation(value = "删除变量表", notes = "删除变量表", produces = "application/json")
        @PostMapping("/delete")
        public JsonResult delete(@Valid @RequestBody VariableDictSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        VariableDictSaveOrUpdateDTO saveDTO = switchToDTO(param, VariableDictSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());
        saveDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = service.deleteVariableDict(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
        return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
        }

}

