package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/6 10:52
 */
@Data
@ToString
public class ProcessVO implements Serializable {

    private static final long serialVersionUID = 8063523172590437934L;

    private Long processId;

    private String processKey;

    private String processName;

    private Long processMenuId;

    private String processAbbr;

    private Long companyId;

    private String companyCode;

    private String tenantId;

    private Integer sort;

    private Integer processType;

    private Long processDetailId;

    private Integer processStatus;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

}
